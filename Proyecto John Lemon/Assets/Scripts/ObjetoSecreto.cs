using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjetoSecreto : MonoBehaviour
{
    public GameObject player;
    public AudioSource win;
    public Transform[] spawnCoin;
    public CanvasGroup exitBackgroundImageCanvasGroup;

    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    bool playerGotObject;
    float m_Timer;

    private void Start()
    {
        transform.position = spawnCoin[Random.Range(0,spawnCoin.Length)].position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player)
        {
            playerGotObject = true;
            win.Play();
        }
    }

    private void Update()
    {
        if (playerGotObject)
        {
            GameEnding();
        }
    }

    void GameEnding()
    {
        m_Timer += Time.deltaTime;

        exitBackgroundImageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            Application.Quit();
        }
    }
}
